open Codelib

(* For the benefit of offshoring, etc. *)
(* Same interface as https://github.com/metaocaml/ber-metaocaml/blob/ber-n114/ber-metaocaml-114/runcode.ml *)
let typecheck_code : 'a closed_code -> Typedtree.expression =
 fun cde ->
  (* let str = typecheck_code' @@ ast_of_code cde in
     match str.Typedtree.str_items with
     | [ { Typedtree.str_desc = Typedtree.Tstr_eval (texp, _); _ } ] -> texp
     | _ -> failwith "cannot happen: Parsetree was not an expression?"
  *)
  cde
