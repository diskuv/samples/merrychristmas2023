type 'a code = Lexing.position * Parsetree.expression * Lexing.position
(** The [code] is [(start_location, expression, end_location)]. Use ppx_here to generate
    the start and end locations (positions in the source code) that surrounds your
    expression. *)

type closed_code_repr = Typedtree.expression
