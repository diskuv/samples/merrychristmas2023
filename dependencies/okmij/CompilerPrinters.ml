(** Pretty-printers for internal compiler types.

   {1 Purpose}

   To print an informative error, we can't let the OCaml compiler
   just print [Failure: Env(_)] when the user has typed something
   wrong.

   So we make our own pretty-printer of existing compiler types.

   {1 Implementation}

   We use the technique at
  https://github.com/ocaml-ppx/ppx_deriving?tab=readme-ov-file#working-with-existing-types
   to add PPX generated pretty-printers to the existing compiler types.

   It would be nice to use [@@deriving refl] as the PPX generator since
   [refl] is already a requirement for Metatype, but that is not possible
   since one of the transitive types ([Ident.t]) is opaque.

   So we will use [@@deriving show]. And because we already have a
   runtime dependency on ppxlib for refl we don't need to inline the
   [show_] and [pp_] generated functions using [@@deriving show][@@@end].
   Inlining is not great anyway because that ties the source code to
   the compiler version. 
   
   {1 Proxy Modules}
*)

module MIdent = struct
  type t = Ident.t (* This is opaque type so no [@@deriving refl] is allowed. *)

  let pp = Ident.print_with_scope
end

module MPath = struct
  type t = Path.t = Pident of MIdent.t | Pdot of t * string | Papply of t * t
  [@@deriving show { with_path = false }]
end

module MLongident = struct
  type t = Longident.t =
    | Lident of string
    | Ldot of t * string
    | Lapply of t * t
  [@@deriving show { with_path = false }]
end

module MLocation = struct
  (* module MLexing = struct
       type position = Lexing.position = {
         pos_fname : string;
         pos_lnum : int;
         pos_bol : int;
         pos_cnum : int;
       }
       [@@deriving show { with_path = false }]
     end *)

  type t = Location.t
  (* Too verbose, we already report location nicely, and Location has a better printer.

     = {
       loc_start : MLexing.position;
       loc_end : MLexing.position;
       loc_ghost : bool;
     }
     [@@deriving show { with_path = false }] *)

  let pp fmt loc =
    if Location.is_none loc then Format.fprintf fmt "<somewhere>"
    else Location.print_loc fmt loc
end

module MEnv = struct
  type t = Env.t

  let pp fmt _v = Format.fprintf fmt "<Env>"

  type unbound_value_hint = Env.unbound_value_hint =
    | No_hint
    | Missing_rec of MLocation.t
  [@@deriving show { with_path = false }]

  type lookup_error = Env.lookup_error =
    | Unbound_value of MLongident.t * unbound_value_hint
    | Unbound_type of MLongident.t
    | Unbound_constructor of MLongident.t
    | Unbound_label of MLongident.t
    | Unbound_module of MLongident.t
    | Unbound_class of MLongident.t
    | Unbound_modtype of MLongident.t
    | Unbound_cltype of MLongident.t
    | Unbound_instance_variable of string
    | Not_an_instance_variable of string
    | Masked_instance_variable of MLongident.t
    | Masked_self_variable of MLongident.t
    | Masked_ancestor_variable of MLongident.t
    | Structure_used_as_functor of MLongident.t
    | Abstract_used_as_functor of MLongident.t
    | Functor_used_as_structure of MLongident.t
    | Abstract_used_as_structure of MLongident.t
    | Generative_used_as_applicative of MLongident.t
    | Illegal_reference_to_recursive_module
    | Cannot_scrape_alias of MLongident.t * MPath.t
  [@@deriving show { with_path = false }]

  type env_error = Env.error =
    | Missing_module of MLocation.t * MPath.t * MPath.t
    | Illegal_value_name of MLocation.t * string
    | Lookup_error of MLocation.t * t * lookup_error
  [@@deriving show { with_path = false }]
end
