module StringMap = Map.Make (String)

let local_identifiers_to_rewrite = ref StringMap.empty

let collect_rewrites modulename l =
  let seq_of_new_rewrites =
    List.map (fun i -> (i, modulename)) l |> List.to_seq
  in
  local_identifiers_to_rewrite :=
    StringMap.add_seq seq_of_new_rewrites !local_identifiers_to_rewrite

(* START: MUST correspond with Offshoring.op_of *)
let () =
  collect_rewrites "Stdlib"
    [
      "+";
      "-";
      "*";
      "/";
      "mod";
      "+.";
      "-.";
      "*.";
      "/.";
      "&&";
      "||";
      "land";
      "lor";
      "~-";
      "~-.";
      "=";
      "<>";
      "<";
      "<=";
      ">";
      ">=";
      "float_of_int";
      "int_of_float";
      "truncate";
      ":=";
      "!";
      "ref";
      "incr";
      "decr";
    ]

let () =
  collect_rewrites "Stdlib.Int"
    [ "logand"; "logor"; "logxor"; "lognot"; "shift_left"; "shift_right" ]

let () = collect_rewrites "Stdlib.Array" [ "make"; "get"; "set" ]
let () = collect_rewrites "Stdlib.Bigarray.Array1" [ "get"; "set" ]
(* END: MUST correspond with Offshoring.op_of *)

(* These are others that are scattered around Oleg's test code
   or that we (Metatype team) simply think should be present. *)
let () = collect_rewrites "Stdlib" [ "min"; "max" ]

let debugging = ref false

let fully_qualify_local_type_identifiers =
  object
    inherit Ppxlib.Ast_traverse.map as super

    method! expression e =
      let e0 = super#expression e in
      match e0.pexp_desc with
      (* Rewrite [localname] to [modulename.localname].
         Example: [+] to [Stdlib.+] *)
      | Pexp_ident ({ txt = Lident ident; _ } as pi) -> (
          match StringMap.find_opt ident !local_identifiers_to_rewrite with
          | None -> e0
          | Some modulename -> begin
              if !debugging then
                Format.printf "%s:%d {Lident} %s\n@." __FILE__ __LINE__ ident;
              {
                e0 with
                pexp_desc =
                  Pexp_ident { pi with txt = Ldot (Lident modulename, ident) };
              }
            end)
      (* Rewrite [Array.get] to [Stdlib.Array.get] *)
      | Pexp_ident ({ txt = Ldot (Lident "Array", fname) as dot; _ } as pi) ->
          if !debugging then
            Format.printf "%s:%d {Ldot Lident} %a\n@." __FILE__ __LINE__
              CompilerPrinters.MLongident.pp dot;
          {
            e0 with
            pexp_desc =
              Pexp_ident
                {
                  pi with
                  txt =
                    Longident.unflatten [ "Stdlib"; "Array"; fname ]
                    |> Option.get;
                };
          }
      (* Rewrite [Bigarray.Array1.get] to [Stdlib.Bigarray.Array1.get] *)
      | Pexp_ident
          ({ txt = Ldot (Ldot (Lident "Bigarray", "Array1"), fname) as dot; _ }
          as pi) ->
          if !debugging then
            Format.printf "%s:%d {Ldot Ldot Lident} %a\n@." __FILE__ __LINE__
              CompilerPrinters.MLongident.pp dot;
          {
            e0 with
            pexp_desc =
              Pexp_ident
                {
                  pi with
                  txt =
                    Longident.unflatten
                      [ "Stdlib"; "Bigarray"; "Array1"; fname ]
                    |> Option.get;
                };
          }
      | _ -> e0
  end

let fully_qualify = fully_qualify_local_type_identifiers#expression
