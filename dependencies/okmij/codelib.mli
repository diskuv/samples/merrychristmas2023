type 'a code = 'a Trx.code
type 'a closed_code = Trx.closed_code_repr

val close_code : 'a code -> 'a closed_code
(** [close_code code] does type inference and type checking.
    
    Raises {!OffshoreError.Typing} if the typing inference
    and checking failed. *)

val pp_code : Format.formatter -> 'a code -> unit
(** [pp_code fmt code] prints the code [code] on [fmt]. *)

val pp_closed_code : Format.formatter -> 'a closed_code -> unit
(** [pp_closed_code fmt code] prints the closed code [code] on [fmt]. *)
