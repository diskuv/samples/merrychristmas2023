exception
  Typing of {
    full_error : string;
    cause : string;
    expression : string;
    loc : Location.t;
  }
