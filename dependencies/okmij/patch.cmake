# This is used by dependencies/vendor.cmake

function(Okmij_PatchFile)
    set(noValues)
    set(singleValues TYPE INFILE OUTFILE)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    file(READ "${ARG_INFILE}" contents)

    # DEVELOPER: Try to make these replacements 1) idempotent and 2) work with any future version from Oleg

    # And expose a function to set it
    if(ARG_TYPE STREQUAL ml)
        set(contents "[@@@warning \"-9\"]
module OffshoreError = OffshoreError
${contents}")
    endif()

    # Would like: file(WRITE "${ARG_OUTFILE}" "${contents}")
    # but that does not write LF endings on Windows.
    file(CONFIGURE OUTPUT "${ARG_OUTFILE}" CONTENT "${contents}" @ONLY NEWLINE_STYLE UNIX)
endfunction()

function(Okmij_PatchAll)
    set(noValues)
    set(singleValues INDIR OUTDIR)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    Okmij_PatchFile(TYPE ml INFILE "${ARG_INDIR}/offshoringIR_pp.ml" OUTFILE "${ARG_OUTDIR}/offshoringIR_pp.ml")
    Okmij_PatchFile(TYPE ml INFILE "${ARG_INDIR}/offshoring.ml" OUTFILE "${ARG_OUTDIR}/offshoring.ml")
endfunction()
