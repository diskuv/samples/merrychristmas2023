module Offshoring = Offshoring
module OffshoringIR = OffshoringIR
module OffshoringIR_pp = OffshoringIR_pp

module Xmas2023 = struct
  module SumArConv = struct
    open Offshoring
    include DefaultConv

    let id_conv : pathname -> string -> string =
     fun pn v ->
      match (pn, v) with
      | "Stdlib", "min" -> "min"
      | _ -> DefaultConv.id_conv pn v
  end

  (* Sadly [Codelib.close_code] or [Codelib.pp_closed_code] destroys the backtrace *)
  let better_backtraces_worse_diagnostics = ref true

  let () =
    (* Only OffshoringIR.TVoid and other [OffshoringIR] types are defined. *)
    OffshoringIR_pp.cnv_typ_ext :=
      function
      | Offshoring.TVariable ->
          (* When a variable is not read, its types can't be found and
             Offshoring has no idea what to print for the variables type! *)
          "TVariable_likely_unused_so_fails_to_unify_to_type"
      | Offshoring.TArrow _ ->
          (* Ditto for arrows (function types), although have not seen in practice *)
          "TArrow_likely_unused_so_fails_to_unify_to_types"
      | _ ->
          Printexc.raise_with_backtrace
            (Failure "A type was not converted in OffshoringIR_pp.cnv_typ_ext")
            (Printexc.get_raw_backtrace ())

  let compile_to_ir converter staged =
    Offshoring.(
      try offshore converter staged
      with OffshoreError.Typing { full_error; _ } as e ->
        prerr_endline ("FATAL: " ^ full_error);
        raise e)

  let print_c name converter staged =
    let ir = compile_to_ir converter staged in
    try
      OffshoringIR_pp.pp_to_c ~out:Format.std_formatter
        ~name:(__MODULE__ ^ "_" ^ name)
        ir
    with Failure _ as e ->
      (* If [compile_to_ir code] succeeded then we know that the
         [code] could be closed without error. So let's print that,
         but only if we are okay with the stack traces being
         destroyed. *)
      if !better_backtraces_worse_diagnostics then
        Format.eprintf
          "@[<v>FATAL: Encountered a problem printing to C.@;\
           @[  The code was:@]@;\
           @[    %a@]@;\
           @]@."
          Codelib.pp_code staged
      else
        Format.eprintf
          "@[<v>FATAL: Encountered a problem printing to C.@;\
           @[  The code was:@]@;\
           @[    %a@]@;\
           @[  The closed code was:@]@;\
           @[    %a@]@;\
           @]@."
          Codelib.pp_code staged Codelib.pp_closed_code
          (Codelib.close_code staged);
      raise e
end

module ForTesting = struct
  module Codelib = Codelib
end
