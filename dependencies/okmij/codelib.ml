open Types

type 'a code = 'a Trx.code
type 'a closed_code = Trx.closed_code_repr

module PendingModule = struct
  type t = {
    module_names : string list;
        (** The hierarchy of module names from the top down to the pending module *)
    signature_ref : signature_item list ref;
  }

  let create ~module_names = { module_names; signature_ref = ref [] }

  let sub_local_name t =
    match List.rev t.module_names with [] -> None | hd :: _ -> Some hd
end

(** Both Stdlib and Dune use the ["__"] naming scheme to ensure
     globally unique module names *)
let unique_module_name ~module_names = String.concat "__" module_names

(** [create_uid ~module_names] creates a compilation unit identifier.

    [module_names] is a non-empty hierarchically ordered list of module
    names, like ["Stdlib"; "Array"].

    {!Uid.of_compilation_unit_id} must not be given
    a Local identifier or else it throws [Misc.Fatal_error].
    Using this function avoids creating a local identifier. *)
let create_uid ~module_names =
  assert (module_names <> []);
  Uid.of_compilation_unit_id
    (Ident.create_persistent (unique_module_name ~module_names))

(** [mk_add_type] is similar to OCaml's ["typing/predef.ml"] which is
    used to define predefined types like [int], and additionally adds
    the type to a module's signature. *)
let mk_add_type add_type (pending_module : PendingModule.t) type_name ?manifest
    ?(immediate = Type_immediacy.Unknown) ?(kind = Type_abstract) ~arity ~params
    ~variance env =
  assert (List.length params = arity);
  assert (List.length variance = arity);
  let local_ident = Ident.create_local type_name in
  let decl =
    {
      type_params = params;
      type_arity = arity;
      type_kind = kind;
      type_loc = Location.none;
      type_private = Asttypes.Public;
      type_manifest = manifest;
      type_variance = variance;
      (* Ocaml_typing.Typedecl_separability:
         The OCaml runtime assumes for type-directed optimizations that all
         types are "separable". A type is "separable" if either all its
         inhabitants (the values of this type) are floating-point numbers
         or none of them are. *)
      type_separability = List.map (fun _ -> Separability.Sep) params;
      type_is_newtype = false;
      type_expansion_scope = Btype.lowest_level;
      type_attributes = [];
      type_immediate = immediate;
      type_unboxed_default = false;
      type_uid = create_uid ~module_names:pending_module.module_names;
    }
  in
  (* Queue the [type <name> = <type>] to the module signature *)
  let items = pending_module.signature_ref in
  items := Sig_type (local_ident, decl, Trec_not, Exported) :: !items;
  (* Add <type> to the environment *)
  add_type local_ident decl env

(** [get_type_expr env core_type] translates the AST type expression [core_type]
    into a typed type expression using inhabitants from the environment [env].
    
    Uses OCaml's "simple" algorithm. *)
let get_type_expr env core_type =
  let _core_type, type_expr, _thunk =
    Typetexp.transl_simple_type_delayed env core_type
  in
  type_expr

(** [mk_add_value ?kind add_value pending_module value_name core_type env]
    adds a value named [value_name] of type [core_type] to the pending
    module [pending_module] in the environment [env].

    The [add_value] parameter is used to update the environment [env]
    with the new value. *)
let mk_add_value add_value (pending_module : PendingModule.t) value_name
    core_type ?(kind = Val_reg) env =
  (* If global values are needed, add case for [val_uid = Uid.of_predef_id]
     and use [add_value (Ident.create_persistent)] *)
  assert (pending_module.module_names <> []);
  let type_expr = get_type_expr env core_type in
  let local_ident = Ident.create_local value_name in
  let decl_val =
    {
      Types.val_loc = Location.none;
      val_attributes = [];
      val_kind = kind;
      val_type = type_expr;
      val_uid = create_uid ~module_names:pending_module.module_names;
    }
  in
  (* Queue the [val <name>: [%type: ...]] to the module signature *)
  let items = pending_module.signature_ref in
  items := Sig_value (local_ident, decl_val, Exported) :: !items;

  (* Add the [[%type: ...]] to the environment *)
  add_value local_ident decl_val env

(** [mk_add_module] adds a top-level module to the environment *)
let mk_add_module add_module (pending_module : PendingModule.t) env =
  let ident =
    Ident.create_persistent
    @@ unique_module_name ~module_names:pending_module.module_names
  in
  let msignature = Mty_signature !(pending_module.signature_ref) in
  add_module ident Mp_present msignature env

(** [add_submodule ~parent ~sub] adds [module <sub>] to the [module <parent>]'s
    signature.
    
    Example with [parent = Stdlib] and [sub = Array]:
    
    {[
      module Stdlib : sig
        module Array : sig end
      end
    ]}
    *)
let add_submodule ~(parent : PendingModule.t) ~(sub : PendingModule.t) =
  let sub_module_names = sub.module_names in
  let sub_local_name = PendingModule.sub_local_name sub |> Option.get in
  let decl =
    {
      md_type = Mty_signature !(sub.signature_ref);
      md_attributes = [];
      md_loc = Location.none;
      (* [of_compilation_unit_id] must not be Local or throws [Misc.Fatal_error] *)
      md_uid = create_uid ~module_names:sub_module_names;
    }
  in
  let items = parent.signature_ref in
  items :=
    Sig_module
      (Ident.create_local sub_local_name, Mp_present, decl, Trec_not, Exported)
    :: !items

(** [standard_env loc] creates an environment of types, values and modules
    that allows [Offshoring] to succeed.
    
    The [loc] location should be the location where ... if there is a typing
    error ... the error can be located. That means for user-supplied type
    transformations the location should be the user's source code. *)
let standard_env loc =
  (* argument: [loc]. Needed by the [%type: ...] PPX expressions. *)

  (* Convenient accessors *)
  let add_type (pending_module : PendingModule.t) =
    mk_add_type (Env.add_type ~check:false) pending_module
  in
  let add_value (pending_module : PendingModule.t) name =
    mk_add_value (Env.add_value ?check:None) pending_module name
  in
  let add_module (pending_module : PendingModule.t) =
    mk_add_module (Env.add_module ?arg:None ?shape:None) pending_module
  in

  (* Initial values *)
  let env = Env.empty in
  let stdlib = PendingModule.create ~module_names:[ "Stdlib" ] in
  let stdlib_int = PendingModule.create ~module_names:[ "Stdlib"; "Int" ] in
  let stdlib_array = PendingModule.create ~module_names:[ "Stdlib"; "Array" ] in
  let stdlib_bigarray =
    PendingModule.create ~module_names:[ "Stdlib"; "Bigarray" ]
  in
  let stdlib_bigarray_array1 =
    PendingModule.create ~module_names:[ "Stdlib"; "Bigarray"; "Array1" ]
  in

  (* In what follows you can use commands like:
       ocamlc -dtypedtree -stop-after typing _opam/src-ocaml/stdlib/int.mli
     to get typing information ...

     But [%type:] is so much better!

     Hover-over with Merlin/OCaml-LSP to copy and paste correct types below. *)
  let q = Stdlib.Array.set in
  ignore q;

  (* Add the predefined types, like what is an [int]. It will _not_ have Stdlib types like [+]. *)
  let env, _unsafe_string_env =
    Predef.build_initial_env
      (Env.add_type ~check:false)
      (Env.add_extension ~check:false ~rebind:false)
      env
  in

  (* Add standard library types *)
  let typ_param_polya = get_type_expr env [%type: 'a] in
  let typ_param_polyb = get_type_expr env [%type: 'b] in
  let typ_param_polyc = get_type_expr env [%type: 'c] in
  let env =
    (* 'a Stdlib.ref (the = { mutable contents: 'a } is ignored ) *)
    add_type stdlib "ref" ~arity:1 ~params:[ typ_param_polya ]
      ~variance:Variance.[ null ]
      env
  in
  let env =
    (* (!'a,!'b,!'c) Stdlib.Bigarray.Array1.t *)
    add_type stdlib_bigarray_array1 "t" ~arity:3
      ~params:[ typ_param_polya; typ_param_polyb; typ_param_polyc ]
      ~variance:(Variance.unknown_signature ~injective:true ~arity:3)
      env
  in

  let add_values env' pending_module names type_expr =
    List.fold_left
      (fun env'' name -> add_value pending_module name type_expr env'')
      env' names
  in

  let env =
    add_values env stdlib
      [ "+"; "-"; "*"; "/"; "land"; "lor" ]
      [%type: int -> int -> int]
  in
  let env =
    add_values env stdlib [ "+."; "-."; "*."; "/." ]
      [%type: float -> float -> float]
  in

  let env =
    add_values env stdlib [ "&&"; "||" ] [%type: bool -> bool -> bool]
  in

  let env =
    add_values env stdlib_int
      [ "logand"; "logor"; "logxor"; "shift_left"; "shift_right" ]
      [%type: int -> int -> int]
  in
  let env = add_values env stdlib_int [ "lognot" ] [%type: int -> int] in

  let env = add_values env stdlib [ "~-" ] [%type: int -> int] in
  let env = add_values env stdlib [ "~-." ] [%type: float -> float] in

  let env =
    (* Yes, we are keeping these polymorphic to conform to [Offshoring.op_of]
       which allows them to be polymorphic over the number types I32 and F64 *)
    add_values env stdlib
      [ "="; "<>"; "<"; "<="; ">"; ">=" ]
      [%type: 'a -> 'a -> bool]
  in

  let env = add_values env stdlib [ "float_of_int" ] [%type: int -> float] in
  let env =
    add_values env stdlib [ "int_of_float"; "truncate" ] [%type: float -> int]
  in

  let env = add_values env stdlib [ ":=" ] [%type: 'a ref -> 'a -> unit] in
  let env = add_values env stdlib [ "!" ] [%type: 'a ref -> 'a] in
  let env = add_values env stdlib [ "ref" ] [%type: 'a -> 'a ref] in
  let env = add_values env stdlib [ "incr"; "decr" ] [%type: int ref -> unit] in

  let env = add_value stdlib_array "get" [%type: 'a array -> int -> 'a] env in
  let env =
    add_value stdlib_array "set" [%type: 'a array -> int -> 'a -> unit] env
  in

  let env =
    add_value stdlib_bigarray_array1 "get" [%type: ('a, 'b, 'c) t -> int -> 'a]
      env
  in
  let env =
    add_value stdlib_bigarray_array1 "set"
      [%type: ('a, 'b, 'c) t -> int -> 'a -> unit] env
  in

  (* Metatype additions (also see QualifyLocalTypes) *)
  let env = add_values env stdlib [ "min"; "max" ] [%type: int -> int -> int] in

  (* Add Stdlib and related submodules in reverse order so signatures are complete *)
  let () = add_submodule ~parent:stdlib_bigarray ~sub:stdlib_bigarray_array1 in
  let () = add_submodule ~parent:stdlib ~sub:stdlib_int in
  let () = add_submodule ~parent:stdlib ~sub:stdlib_array in
  let () = add_submodule ~parent:stdlib ~sub:stdlib_bigarray in
  let env = add_module stdlib env in

  env

let close_code (code : 'a code) : 'a closed_code =
  (* Adjust location of code to what user specified *)
  let loc_start, expr, loc_end = code in
  let loc : Location.t =
    if Location.is_none expr.pexp_loc then
      { loc_ghost = false; loc_start; loc_end }
    else expr.pexp_loc
  in
  let expr = { expr with pexp_loc = loc } in
  (* Fully qualify local types to be compliant with [Offshoring] *)
  let expr = QualifyLocalTypes.fully_qualify expr in
  (* Setup failure handler *)
  let fail cause orig_backtrace =
    (* [expr] is the FULLY-QUALIFIED expression. Not exactly the same as the source
       code at [loc]. We print both. *)
    let msg =
      Format.asprintf
        "@[<v>@[Offshoring failed.@]@;\
         @[Cause: %s@]@;\
         @[<v>Expression: %a@;\
        \  %a@]@]" cause Location.print_loc loc Pprintast.expression expr
    in
    let expression = Format.asprintf "%a" Pprintast.expression expr in
    Printexc.raise_with_backtrace
      (OffshoreError.Typing { full_error = msg; cause; expression; loc })
      orig_backtrace
  in
  (* Do type inference (which does type checking) *)
  let which_problem = ref "The Metatype Standard environment" in
  try
    (* Create types, values and modules for [Offshoring] *)
    let env = standard_env loc in
    (* Type inference *)
    which_problem := "Your expression";
    Typecore.type_expression env expr
  with
  (*
       Nice printing of errors
    *)
  | Typecore.Error (_loc, env, error) ->
      let report = Typecore.report_error ~loc env error in
      let msg =
        Format.asprintf "@[<v>%s had a core typing error:@ @[  %a@]@]"
          !which_problem Location.print_report report
      in
      fail msg (Printexc.get_raw_backtrace ())
  | Env.Error e ->
  match e with
  | Lookup_error (_, _, Unbound_value (Longident.Lident ident, _)) ->
      let msg =
        Printf.sprintf
          "%s refers to an value (an operator, variable or function) outside \
           of the Metatype environment: '%s'"
          !which_problem ident
      in
      fail msg (Printexc.get_raw_backtrace ())
  | _ ->
      let msg =
        Format.asprintf "@[<v>%s failed in an unknown way:@ @[  %a@]@]"
          !which_problem CompilerPrinters.MEnv.pp_env_error e
      in
      fail msg (Printexc.get_raw_backtrace ())

let pp_code fmt ((_start, expr, _end) : 'a code) = Pprintast.expression fmt expr

let pp_closed_code fmt (closed_code : 'a closed_code) =
  (* Same as [Offshoring.print_texp] *)
  let open Typedtree in
  Printtyped.implementation fmt
    {
      str_type = [];
      str_final_env = closed_code.exp_env;
      str_items =
        [
          {
            str_loc = closed_code.exp_loc;
            str_env = closed_code.exp_env;
            str_desc = Tstr_eval (closed_code, []);
          };
        ];
    }
