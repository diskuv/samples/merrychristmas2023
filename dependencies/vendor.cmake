# Run with: cmake -P dependencies/vendor.cmake

include("${CMAKE_CURRENT_LIST_DIR}/okmij/patch.cmake")

set(BERMETAOCAML_ARCHIVEFILE ${CMAKE_CURRENT_LIST_DIR}/_temp/bermo.tar.gz)
set(BERMETAOCAML_ARCHIVESPACE ${CMAKE_CURRENT_LIST_DIR}/_temp/bermo)
set(BERMETAOCAML_LICENSE_HTML ${CMAKE_CURRENT_LIST_DIR}/_temp/bermo-license.html)

set(OKMIJ_VENDORDIR ${CMAKE_CURRENT_LIST_DIR}/okmij)

# -----------
# 1. Download
# -----------

# BER MetaOCaml is not versioned! So we use EXPECTED_HASH to check if we are downloading the 4.14.1 version.
file(DOWNLOAD "https://okmij.org/ftp/ML/ber-metaocaml.tar.gz" "${BERMETAOCAML_ARCHIVEFILE}" SHOW_PROGRESS
    EXPECTED_HASH SHA256=befab7c48e23480b2b5dc0e4f7b4cf6c50d880ef4d71078dff205c51b71bff2d)
#   Oleg places his public domain license on his homepage
file(DOWNLOAD "https://okmij.org/ftp/index.html" "${BERMETAOCAML_LICENSE_HTML}")

# -----------
# 2. Pull out the LICENSE and other files and subfolders
# -----------

file(REMOVE_RECURSE "${BERMETAOCAML_ARCHIVESPACE}")
file(ARCHIVE_EXTRACT INPUT "${BERMETAOCAML_ARCHIVEFILE}" DESTINATION "${BERMETAOCAML_ARCHIVESPACE}"
    PATTERNS */c_ast.mli */c_pp.ml */offshoring*.ml*)

# -----------
# 3. Find the archive subdir
# -----------

foreach(varprefix IN ITEMS BERMETAOCAML)
    file(GLOB ${varprefix}_CONTENTSUBDIR LIST_DIRECTORIES true RELATIVE "${${varprefix}_ARCHIVESPACE}" "${${varprefix}_ARCHIVESPACE}/*")
    list(LENGTH ${varprefix}_CONTENTSUBDIR ${varprefix}_CONTENTSUBDIR_LEN)
    if(NOT ${varprefix}_CONTENTSUBDIR_LEN EQUAL 1)
        message(FATAL_ERROR "Expected only one top-level archive directory in ${${varprefix}_ARCHIVESPACE}, not ${${varprefix}_CONTENTSUBDIR}")
    endif()
    set(${varprefix}_CONTENTDIR "${${varprefix}_ARCHIVESPACE}/${${varprefix}_CONTENTSUBDIR}")    
endforeach()

# -----------
# 4. Do patches
# -----------

Okmij_PatchAll(INDIR "${BERMETAOCAML_CONTENTDIR}" OUTDIR "${OKMIJ_VENDORDIR}")

# -----------
# 5. Copy
# -----------

# okmij

file(GLOB BERMETAOCAML_SOURCES LIST_DIRECTORIES false "${BERMETAOCAML_CONTENTDIR}/*.ml" "${BERMETAOCAML_CONTENTDIR}/*.mli")
file(COPY ${BERMETAOCAML_SOURCES} DESTINATION "${OKMIJ_VENDORDIR}"
    PATTERN offshoring.ml EXCLUDE
    PATTERN offshoringIR_pp.ml EXCLUDE)
file(COPY_FILE "${BERMETAOCAML_LICENSE_HTML}" "${OKMIJ_VENDORDIR}/LICENSE.html" ONLY_IF_DIFFERENT)
