# OCaml as the Universal Translator - DkSDK Merry Christmas 2023

December 21, 2023

I'm writing this article a few hours before I start receiving Christmas visitors. So if it feels a bit rushed, it is. But this afternoon I found an application of OCaml that can be summed up in one word: **awesome**.

So here is some context. While a high school team was early adopting my [DkSDK development kit](https://diskuv.com/pricing) -- a dev kit to tame the complexity of multi-platform and multi-language software -- they ran into problems that I had not anticipated. I encourage you to watch a YouTube video ["Develop Cross-Platform Software using 2+ Languages"](https://www.youtube.com/watch?v=N9fVAIPMfKU) that goes in more detail. But I'll save you some time. They had developed an Android app for members of a [robotics team](https://www.firstinspires.org/robotics/frc) to gather intelligence on other robotics teams; yep, the robotics leagues can be quite competitive. Several team members would use their own Android phones to enter intelligence on the Android app, and then the Android app would send the intelligence to a Windows desktop program via QR codes. And their first release was built on a mashup of Android/Java, Qt/C++ and OCaml software.

The team developers had found that they were having to **duplicate** the same collection of fields five (5) different times **all with slightly different variations**:

- fields for an OCaml type
- fields for a Cap n' Proto schema for connectivity (think Protobuf or gRPC)
- fields for an Android immutable [UiState](https://developer.android.com/topic/architecture/ui-layer/stateholders) class
- fields for in-place mutable edits of the above UiState
- fields for a SQLite3 database table

That was tedious and they asked for a **"drastic QOL (quality of life) improvement"**. My key technical requirements were to:

1. Have them write the field definitions in one place.
2. Have them write any variations (transformations) to those fields in one place.

While I was researching how I would deliver the improvement, I came across and purchased Oleg Kiselyov's preprint paper ["Generating C: Heterogeneous metaprogramming system description"](https://www.sciencedirect.com/science/article/abs/pii/S0167642323000977) five (5) days ago. It describes and contrasts two (new) techniques:

1. Using an embedded domain specific language ([DSL](https://en.wikipedia.org/wiki/Domain-specific_language)) that can be translated to C, Java and other languages
2. Using modifications to OCaml to translate a small but powerful subset of OCaml into C, Java and those other languages

Although I like and use DSLs, it sounded like having my users continue to write important parts of their code in OCaml (the second option) would be the best option.

Let's see it! If you have [`opam`](https://ocaml.org/install) installed you can follow along (the `#require ...` line is not a comment):

```shell
$ opam switch create . 4.14.1 # or on Windows: dkml init
$ opam install . utop --with-test --yes
$ opam exec -- dune utop
#require "metaquot.ppx";;
open DkSDKMetatype_Offshore;;
open Xmas2023;;
let sum_ar_staged =
  ( Lexing.dummy_pos,
    [%expr
      fun arr n ->
        let sum = ref 0 in
        for i = 0 to 3 do
          for j = i to min (i + 3) (n - 1) do
            sum := !sum + arr.(j)
          done
        done;
        !sum],
    Lexing.dummy_pos ) ;;

print_c "sum_ar" (module SumArConv) sum_ar_staged ;;
```

which prints:

```c
int DkSDKMetatype_Offshore_sum_ar(int * const arr,int const n){
  int sum = 0;
  for (int i = 0; i < (1 + 3); i += 1)
    for (int j = i; j < (1 + min(i + 3,n - 1)); j += 1)
    sum = sum + (arr[j]);
  return sum;
}
```

Non-trivial, real OCaml (no DSL!). Auto-generated C. More languages easy to add. **AWESOME**.

Merry Christmas! Jonah Beckford

*Edit*: Hacker News - https://news.ycombinator.com/item?id=38748249

*Please share wherever you get your news.*

## Licenses

The main body of work is licensed under [OSL-3.0](./LICENSE). For now I'm not accepting contributing because I intend to change the license; this git repository is just a quick, ready-to-use clone of pieces of larger software. And I've done a terrible job of separating my contributions (currently about 50%) from other code (see below).

The foundational code has its own licenses:

| Project               | License                                       |
| --------------------- | --------------------------------------------- |
| `okmij` Oleg Kiselyov | [Public Domain](./dependencies/okmij/LICENSE) |
| BER MetaOcaml         | [Public Domain](./dependencies/okmij/LICENSE) |
