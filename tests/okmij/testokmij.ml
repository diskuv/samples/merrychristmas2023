(* Based on https://okmij.org/ftp/meta-programming/tutorial/offshore_simple.ml *)
open DkSDKMetatype_Offshore
open Xmas2023

(* (oleg) trivial staging *)
let addv_staged =
  ( [%here],
    [%expr
      fun n vout v1 v2 ->
        for i = 0 to n - 1 do
          vout.(i) <- v1.(i) + v2.(i)
        done],
    [%here] )

(* (oleg) Loop with a stride, alongside the regular OCaml loop *)
(* (oleg) The example is a bit contrived; but it actually an intermediate
   step in a transformation. The inner loop is then unrolled,
   after which we apply scalar conversion. See the full example in
   addv.ml
*)
let sum_ar_staged =
  ( [%here],
    [%expr
      fun arr n ->
        let sum = ref 0 in
        (* forloop 0 n 4 (fun i ->
            for j = i to min (i + 3) (n - 1) do
              sum := !sum + arr.(j)
            done); *)
        for i = 0 to 3 do
          for j = i to min (i + 3) (n - 1) do
            sum := !sum + arr.(j)
          done
        done;
        !sum],
    [%here] )

(* ======================== *)

let () = print_endline "Start IR printing to C"
let () = print_c "addv" (module Offshoring.DefaultConv) addv_staged
let () = print_c "sum_ar" (module SumArConv) sum_ar_staged
let () = print_endline "Done IR printing to C"
